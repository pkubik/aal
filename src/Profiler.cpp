/*
 * AAL-3-LS ubrania
 * Profiler.cpp
 *      Author: Pawel Kubik
 */

#include "Profiler.h"
#include "Problem.h"

namespace aal {

void Profiler::test() {
    if (minN > maxN)
        this->maxN = minN;
    results.clear();
    for (unsigned i = minN; i <= maxN; i += 20) {
        for (unsigned k = 0; k < repeats; ++k) {
            auto problem = Problem(i, density);
            problem.solve();
            results.push_back(std::make_pair(i, problem.getSolvingTime()));
        }
    }
}

} /* namespace aal */
