/*
 * AAL-3-LS ubrania
 * DotParser.cpp
 *      Author: Pawel Kubik
 */

#include "DotParser.h"

namespace aal {

DotReader::DotReader(const std::string& filename) : stream(filename) {
    std::string keyword, bracket;
    stream >> keyword >> name >> bracket;

    if (keyword != "graph" || bracket != "{") {
        stream.close();
    }
}

DotParser::Edge DotReader::readEdge() {
    std::string keyword;
    Edge edge;
    stream >> edge.start >> keyword >> edge.end;
    if (edge.start == "}" || keyword != "--") {
        stream.close();
    }
    return edge;
}

DotWriter::DotWriter(const std::string& name, const std::string& filename) : stream(filename) {
    this->name = name;
    stream << "graph " << name << " {" << std::endl;
}

void DotWriter::writeEdge(const Edge& edge) {
    stream << INDENT << edge.start << EDGE << edge.end << std::endl;
}

void DotWriter::writeCluster(const std::string& clusterName, const std::vector<std::string>& vertices) {
    stream << INDENT << "subgraph cluster_" << clusterName << " {" << std::endl
           << INDENT << INDENT;
    for (auto v : vertices) {
        stream << v << " ";
    }
    stream << std::endl
           << INDENT << "}" << std::endl;
}

void DotWriter::close() {
    if (stream.is_open()) {
        stream << "}";
        stream.close();
    }
}

} /* namespace aal */
