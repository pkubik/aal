/*
 * AAL-3-LS ubrania
 * Problem.cpp
 *      Author: Pawel Kubik
 */

#include "Problem.h"
#include <chrono>
#include <algorithm>
#include <functional>
#include <queue>
#include <random>

namespace aal {

Problem::Problem(DotReader& reader) {
    DotParser::Edge edge = reader.readEdge();
    while (reader) {
        graph.addEdge(edge.start, edge.end);
        edge = reader.readEdge();
    }
}

Problem::Problem(unsigned n, float den) {
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_real_distribution<double> distribution(0.0, 1.0);
    for (unsigned i = 0; i < n; ++i) {
        graph.addVertex(std::to_string(i));
    }
    for (unsigned i = 0; i < n; ++i) {
        for (unsigned j = 0; j < n; ++j) {
            double r = distribution(mt);
            if (i != j && r < den) {
                graph.addEdge(i, j);
            }
        }
    }
}

void Problem::solve() {
    auto begin = std::chrono::high_resolution_clock::now();
    invertGraph();
    clusterize();
    solvingTime = std::chrono::duration_cast<std::chrono::nanoseconds>(
            std::chrono::high_resolution_clock::now() - begin).count();
}

void Problem::writeGraph(DotWriter& writer) {
    auto edges = graph.getEdges();
    for (unsigned i = 0; i < edges.size(); ++i) {
        for (auto v : edges[i]) {
            if (v > i) {
                writer.writeEdge(DotParser::Edge{graph.getVertex(i), graph.getVertex(v)});
            }
        }
    }
}

void Problem::writeClusters(DotWriter& writer) {
    for (unsigned i = 0; i < clusters.size(); ++i) {
        writeCluster(std::to_string(i), clusters[i], writer);
    }
}
void Problem::invertGraph() {
    auto begin = std::chrono::high_resolution_clock::now();
    graph.invert();
    inversionTime = std::chrono::duration_cast<std::chrono::nanoseconds>(
            std::chrono::high_resolution_clock::now() - begin).count();
    inverted = !inverted;
}

void Problem::clusterize() {
    auto begin = std::chrono::high_resolution_clock::now();

    const unsigned n = graph.getVerticesNumber();
    unsigned clusterNumber = 0;
    /* Which cluster does given vertex belong to */
    std::vector<int> clusterization(n, -1);
    auto edges = graph.getEdges();

    for (unsigned i = 0; i < n; ++i) {
        if (clusterization[i] == -1) {
            /* Find all vertex from connected component containing vertex v */
            std::queue<unsigned> fringe;
            clusterization[i] = clusterNumber;
            fringe.push(i);
            while (!fringe.empty()) {
                unsigned v = fringe.front();
                fringe.pop();
                for (auto neighbour : edges[v]) {
                    if (clusterization[neighbour] == -1) {
                        fringe.push(neighbour);
                        clusterization[neighbour] = clusterNumber;
                    }
                }
            }
            ++clusterNumber;
        }
    }

    clusterizationTime = std::chrono::duration_cast<std::chrono::nanoseconds>(
            std::chrono::high_resolution_clock::now() - begin).count();

    clusters.resize(clusterNumber);
    for (unsigned i = 0; i < n; ++i) {
        clusters[clusterization[i]].push_back(i);
    }
}

void Problem::writeCluster(const std::string& name, const std::vector<unsigned>& vertices, DotWriter& writer) {
    std::vector<std::string> verticesNames(vertices.size());
    std::transform(vertices.begin(), vertices.end(), verticesNames.begin(),
                   [&] (unsigned x) { return graph.getVertex(x); });
    writer.writeCluster(name, verticesNames);
}

} /* namespace aal */
