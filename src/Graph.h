/*
 * AAL-3-LS ubrania
 * Graph.h
 *      Author: Pawel Kubik
 */

#pragma once

#include <vector>
#include <set>
#include <unordered_map>

namespace aal {

/**
 * Undirected graph representation
 */
class Graph {
    std::vector<std::string> vertices;
    std::vector< std::set<unsigned> > edges;
    std::unordered_map< std::string, unsigned > vertexByName;
public:
    unsigned getVerticesNumber() const { return edges.size(); }

    template < typename CollectionType = std::set<unsigned> >
        CollectionType enumerateVertices() const;

    const std::string& getVertex(unsigned index) const { return vertices[index]; }
    unsigned addVertex(const std::string& name);
    void addEdge(unsigned v1, unsigned v2);
    void addEdge(const std::string& v1, const std::string& v2);
    const std::vector< std::set<unsigned> >& getEdges() const { return edges; }
    /**
     * Invert this graph
     */
    void invert();
};

} /* namespace aal */
