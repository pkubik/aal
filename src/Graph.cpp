/*
 * AAL-3-LS ubrania
 * Graph.cpp
 *      Author: Pawel Kubik
 */

#include "Graph.h"
#include <algorithm>

namespace aal {

template < typename CollectionType >
CollectionType Graph::enumerateVertices() const {
    CollectionType vertices;
    auto verticesNumber = getVerticesNumber();
    for(unsigned i = 0; i < verticesNumber; ++i)
    {
        vertices.insert(i);
    }
    return vertices;
}

unsigned Graph::addVertex(const std::string& name) {
    auto p = vertexByName.insert(make_pair(name, vertices.size()));
    if (p.second) {
        vertices.push_back(name);
        edges.emplace_back();
    }
    return p.first->second;
}

void Graph::addEdge(unsigned v1, unsigned v2) {
    edges[v1].insert(v2);
    edges[v2].insert(v1);
}

void Graph::addEdge(const std::string& v1, const std::string& v2) {
    auto i1 = addVertex(v1);
    auto i2 = addVertex(v2);
    addEdge(i1, i2);
}

void Graph::invert() {
    auto all_vertices = enumerateVertices();
    for (auto it = edges.begin(); it != edges.end(); ++it) {
        std::set<unsigned> result;
        std::set_difference(all_vertices.begin(), all_vertices.end(),
                            it->begin(), it->end(), std::inserter(result, result.begin()));
        it->swap(result);
    }
}

} /* namespace aal */
