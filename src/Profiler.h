/*
 * AAL-3-LS ubrania
 * Profiler.h
 *      Author: Pawel Kubik
 */

#pragma once

#include <vector>
#include <algorithm>

namespace aal {

/**
 * Class to perform multiple timing tests
 */
class Profiler {
    typedef std::vector< std::pair<unsigned, long long> > Results;
    Results results;
public:
    unsigned minN = 10;
    unsigned maxN = 1000;
    unsigned repeats = 1;
    float density = 0.5;
    void test();
    const Results& getResults() const { return results; }
};

} /* namespace aal */
