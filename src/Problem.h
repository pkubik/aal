/*
 * AAL-3-LS ubrania
 * Problem.h
 *      Author: Pawel Kubik
 */

#pragma once

#include "Graph.h"
#include "DotParser.h"

namespace aal {

/**
 * Problem representation
 */
class Problem {
    Graph graph;
    /** State whether graph is already inverted or not */
    bool inverted = false;
    /** Collection of connected components within inverted input graph */
    std::vector< std::vector<unsigned> > clusters;
    /** last graph inversion duration in nanoseconds */
    long long inversionTime = 0;
    /** duration of finding all connected components in nanoseconds */
    long long clusterizationTime = 0;
    /** overall duration of finding the solution of this problem */
    long long solvingTime = 0;

    void invertGraph();
    /**
     * find all connected components in the graph
     */
    void clusterize();
    void writeCluster(const std::string& name, const std::vector<unsigned>& vertices, DotWriter& writer);
public:
    /**
     * Read problem description from file
     * @param reader DotReader pointed to dot file
     */
    Problem(DotReader& reader);
    /**
     * Generate random instance of problem
     * @param n number of vertices
     * @param den probability of an edge between every two vertices
     */
    Problem(unsigned n, float den = 0.75f);
    void solve();
    void writeGraph(DotWriter& writer);
    void writeClusters(DotWriter& writer);
    long long getInversionTime() { return inversionTime; }
    long long getClusterizationTime() { return clusterizationTime; }
    long long getSolvingTime() { return solvingTime; }
};

} /* namespace aal */

