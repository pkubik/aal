/*
 * AAL-3-LS ubrania
 * DotParser.cpp
 *      Author: Pawel Kubik
 */

#include <iostream>
#include "../DotParser.h"
#include "../Graph.h"

using namespace std;
using namespace aal;

int main(int argc, char* argv[]) {
    if (argc < 2) {
        return 0;
    }

    DotReader reader(argv[1]);
    cout << reader.getGraphName() << endl;

    Graph graph;

    DotParser::Edge edge = reader.readEdge();
    while (reader) {
        graph.addEdge(edge.start, edge.end);
        edge = reader.readEdge();
    }

    auto edges = graph.getEdges();
    for (unsigned i = 0; i < edges.size(); ++i) {
        cout << graph.getVertex(i) << " -- {";
        for (auto v : edges[i]) {
            cout << graph.getVertex(v) << ", ";
        }
        cout << "}\n";
    }

    graph.invert();
    cout << "Inverted:\n";

    edges = graph.getEdges();
    for (unsigned i = 0; i < edges.size(); ++i) {
        cout << graph.getVertex(i) << " -- {";
        for (auto v : edges[i]) {
            cout << graph.getVertex(v) << ", ";
        }
        cout << "}\n";
    }

    return 0;
}


