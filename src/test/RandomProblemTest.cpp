/*
 * AAL-3-LS ubrania
 * DotWriterTest.cpp
 *      Author: Pawel Kubik
 */

#include <iostream>
#include <string>
#include "../DotParser.h"
#include "../Graph.h"
#include "../Problem.h"

using namespace std;
using namespace aal;

int main(int argc, char* argv[]) {
    string output;

    if (argc < 2) {
        return 0;
    }
    else if (argc < 3) {
        output = "random.out";
    }
    else {
        output = argv[2];
    }

    unsigned n = std::stoi(argv[1]);

    DotWriter writer("random", output);

    Problem problem(n, 0.2f);
    problem.writeGraph(writer);
    problem.solve();
    problem.writeClusters(writer);

    writer.close();

    return 0;
}


