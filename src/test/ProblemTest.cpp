/*
 * AAL-3-LS ubrania
 * DotWriterTest.cpp
 *      Author: Pawel Kubik
 */

#include <iostream>
#include <string>
#include "../DotParser.h"
#include "../Graph.h"
#include "../Problem.h"

using namespace std;
using namespace aal;

int main(int argc, char* argv[]) {
    if (argc < 2) {
        return 0;
    }

    DotReader reader(argv[1]);
    string out(argv[1]);
    out = out.substr(0, out.rfind('.')) + ".out";
    DotWriter writer(reader.getGraphName(), out);

    Problem problem(reader);
    problem.writeGraph(writer);
    problem.solve();
    problem.writeClusters(writer);

    writer.close();

    return 0;
}


