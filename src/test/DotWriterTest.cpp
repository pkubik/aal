/*
 * AAL-3-LS ubrania
 * DotWriterTest.cpp
 *      Author: Pawel Kubik
 */

#include <iostream>
#include <string>
#include "../DotParser.h"
#include "../Graph.h"

using namespace std;
using namespace aal;

int main(int argc, char* argv[]) {
    if (argc < 2) {
        return 0;
    }

    DotReader reader(argv[1]);

    Graph graph;

    DotParser::Edge edge = reader.readEdge();
    while (reader) {
        graph.addEdge(edge.start, edge.end);
        edge = reader.readEdge();
    }

    string out(argv[1]);
    out = out.substr(0, out.rfind('.')) + ".out";

    DotWriter writer(reader.getGraphName(), out);
    auto edges = graph.getEdges();
    for (unsigned i = 0; i < edges.size(); ++i) {
        for (auto v : edges[i]) {
            if (v > i) {
                writer.writeEdge(DotParser::Edge{graph.getVertex(i), graph.getVertex(v)});
            }
        }
    }
    unsigned n = graph.getVerticesNumber();
    vector<string> cluster;
    for (unsigned i = 0; i < n/2; ++i) {
        cluster.push_back(graph.getVertex(i));
    }
    writer.writeCluster("1", cluster);

    writer.close();

    return 0;
}


