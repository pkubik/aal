/*
 * AAL-3-LS ubrania
 * DotReaderTest.cpp
 *      Author: Pawel Kubik
 */

#include <iostream>
#include "../DotParser.h"

using namespace std;
using namespace aal;

int main(int argc, char* argv[]) {
    if (argc < 2) {
        return 0;
    }

    DotReader reader(argv[1]);
    cout << reader.getGraphName() << endl;

    DotParser::Edge edge;

    edge = reader.readEdge();
    while (reader) {
        cout << edge.start << " - " << edge.end << endl;
        edge = reader.readEdge();
    }

    return 0;
}


