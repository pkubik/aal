/*
 * AAL-3-LS ubrania
 * DotParser.h
 *      Author: Pawel Kubik
 */

#pragma once

#include <fstream>
#include <string>
#include <set>
#include <vector>

namespace aal {

/**
 * Simplified graphviz dot format parser
 */
class DotParser {
 protected:
    std::string name;
 public:
    static constexpr auto INDENT = "    ";
    static constexpr auto EDGE = " -- ";
    struct Edge {
        std::string start;
        std::string end;
        Edge() {}
        Edge(const std::string& start, const std::string& end) : start(start), end(end) {}
    };

    const std::string& getGraphName() { return name; }
};

class DotReader : public DotParser {
    std::ifstream stream;
 public:
    DotReader(const std::string& filename);
    operator bool() const { return stream; }
    Edge readEdge();
};

class DotWriter : public DotParser {
    std::ofstream stream;
 public:
    DotWriter(const std::string& name, const std::string& filename);
    void writeEdge(const Edge& edge);
    void writeCluster(const std::string& clusterName, const std::vector<std::string>& vertices);
    void close();
    ~DotWriter() { close(); }
};

} /* namespace aal */
