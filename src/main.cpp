/*
 * AAL-3-LS ubrania
 * main.h
 *      Author: Pawel Kubik
 */

#include <iostream>
#include <string>
#include <cstring>
#include "DotParser.h"
#include "Problem.h"
#include "Profiler.h"

using namespace std;
using namespace aal;

void printUsage() {
    cout <<
        "The program works in 2 modes:\n\n"
        "solve <input path>:\n"
        "\tsolves problem specified in the dot file and outputs new dot file with solution \n"
        "test <n> <den> [<output path>]:\n"
        "\tgenerates random problem-graph of given size and density and solves it \n"
        "time <min n> <max n> <den> <output path>:\n"
        "\tsolves series of problems of given maximal and minimal size and density and write output to file\n";
}

void solve(const char input[]) {
    DotReader reader(input);
    string out(input);
    out = out.substr(0, out.rfind('.')) + ".out";
    DotWriter writer(reader.getGraphName(), out);

    Problem problem(reader);
    problem.writeGraph(writer);
    problem.solve();
    problem.writeClusters(writer);

    writer.close();
}

void test(unsigned n, float den, const char output[]) {
    DotWriter writer("random", output);

    Problem problem(n, den);
    problem.writeGraph(writer);
    problem.solve();
    problem.writeClusters(writer);

    writer.close();
}

void time(unsigned minN, unsigned maxN, float den, const char output[]) {
    Profiler profiler;
    profiler.minN = minN;
    profiler.maxN = maxN;
    profiler.density = den;
    profiler.repeats = 1;
    profiler.test();
    auto results = profiler.getResults();

    std::ofstream stream(output);
    for (auto result : results) {
        stream << result.first << "\t" << result.second / 1000 << endl;
    }
}

int main(int argc, char* argv[]) {
    if (argc == 6 && !strcmp(argv[1], "time")) {
        const auto minN = stoi(argv[2]);
        const auto maxN = stoi(argv[3]);
        const auto den = stof(argv[4]);
        time(minN, maxN, den, argv[5]);
    }
    else if (argc >= 4 && !strcmp(argv[1], "test")) {
        const unsigned n = std::stoi(argv[2]);
        const float den = std::stof(argv[3]);
        if (argc == 5)
            test(n, den, argv[4]);
        else
            test(n, den, "random.out");
    }
    else if (argc == 3 && !strcmp(argv[1], "solve")) {
        solve(argv[2]);
    }
    else {
        printUsage();
    }

    return 0;
}
