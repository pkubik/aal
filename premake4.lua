solution "ubrania"
    configurations { "Debug", "Release" }

    configuration "linux"
        buildoptions { "-std=c++11" }

    configuration { "Debug" }
        targetdir "build"
        location "build"
        defines { "DEBUG" }
        flags { "Symbols" }

    configuration { "Release" }
        targetdir ( "build" )
        location "build"
        defines { "NDEBUG" }
        flags { "Optimize" }

    project "common"
        kind "StaticLib"
        language "C++"
        files { "src/*.h", "src/*.cpp" }
        excludes { "src/main.cpp" }

    project "ubrania"
        kind "ConsoleApp"
        language "C++"
        files { "src/main.cpp" }
        links { "common" }

    require 'lfs'
    for test in lfs.dir("src/test") do
        if test ~= "." and test ~= ".." and string.sub(test, -4) == ".cpp" then
            p = string.sub(test,1,-5)
            io.write(p .. "\n")
            project(p)
                kind "ConsoleApp"
                language "C++"
                files { "src/test/" .. test }
                links { "common" }
        end
    end

