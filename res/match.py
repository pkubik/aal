#! /usr/bin/python3

import sys

def pre(n):
    return n * n

def main():
    if len(sys.argv) < 2:
        exit
    file = open(sys.argv[1], 'r')
    ntT = []
    for i in file.readlines():
        v = i.split()
        n = int(v[0])
        t = int(v[1])
        ntT.append((n,t,pre(n)))
    med = ntT[len(ntT)//2]
    c = med[2]/med[1]
    q = [t*c/T for n,t,T in ntT]
    for i in q:
        print(i)

if __name__ == "__main__":
    main()
