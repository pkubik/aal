#!/bin/bash

for file in *.out; do
    dot -Tsvg "$file" > "`basename $file .out`.svg"
done
