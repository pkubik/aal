###
## Zadanie
AAL-3-LS ubrania
Posiadasz kolekcje n ubrań. O każdej parze dwóch ubrań potrafisz powiedzieć, czy pasują do siebie. Należy rozmieścić ubrania w jak największej liczbie szaf w taki sposób, aby wybierając po jednej dowolnej rzeczy z każdej szafy, mieć gwarancję, że pasują do siebie.


###
## Autor
Paweł Kubik <pkubik@mion.elka.pw.edu.pl>
Politechnika Warszawska
Wydział Elektroniki i Technik Informacyjnych
Inżynieria Systemów Informatycznych


###
## Katalogi
src/ - katalog plików źródłowych, zawiera CMakeList.txt
src/test - katalog plików źródłowych z ręcznymi testami
res/ - katalog plików testowych


###
## Budowanie
Linux:
Uruchomić polecenie 'premake4 gmake' w katalogu głównym, a następnie przejść do katalogu build i wykonać 'make'

Na przykład:

premake4 gmake
cd build
make

Windows:
Uruchomić polecenie 'premake4 vs2010' w katalogu głównym, a następnie przejść do katalogu build i otworzyć projekt VS, aby go zbudować


###
## Uruchamianie
Uruchomić plik wykonywalny ubrania lub ubrania.exe z katalogu budowania


